from dolfin import *
import numpy as np
import matplotlib.pyplot as plt

"""
This demo program solves the Foppl-von Karman equations on an
annular plate of outer radius Re and inner radius Ri. 
The plate is fully clamped in the transverse direction. 
The inner boundary is traction-free and the outer is
subjected to an increasing in-plane displacement forcing it
to deform into an ellipse of semiaxes Re and Re - U.

It is provided as supplementary material to the article:

M. Brunetti, A. Favata, A. Paolone, S. Vidoli. A mixed variational
principle for the Foppl-von Karman equations. Submitted.

It has been implemented in Python using the FEniCS framework:

https://fenicsproject.org/

We consider the following dimensionless quantities:

U = (Re/t^2)*in-plane displacement
w = (1/t)*transverse displacement
phi = (1/D)*airy stress function
f = (Re^4/(D*t))*tranverse force per unit area

with t the thickness and D the bending stiffness.

Dolfin version: 2018.1.0
"""

# Define the Geometry
Re = 1.0 # outer radius
Ri = 0.1 # inner radius

# Define the material properties
nu = Constant(0.30) # Poisson modulus
c_nu = 12.0*(1.0 - nu**2)

# Define the penalty parameters
eta_w, eta_ph = Constant(100.), Constant(100.)

# Define the transverse load (imperfection)
f = Constant(5.0)

# Definet the in-plane displacement at the boundary
ub = Expression(("0.0","-U*x[1]"), U=0.0, degree=1)

# Define the mesh
mesh = Mesh("annular-mesh.xml")

# Compute cell average size and facet normals
h = CellDiameter(mesh)
h_avg = (h('+') + h('-'))/2.0
n = FacetNormal(mesh)

# Define the boundaries	
class Inner(SubDomain):
	def inside(self, x, on_boundary):
		return on_boundary and sqrt((x[0])**2 + (x[1])**2) - Ri <= mesh.hmin()
	
class Outer(SubDomain):
	def inside(self, x, on_boundary):
		return on_boundary and Re - sqrt((x[0])**2 + (x[1])**2) <= mesh.hmin()

# Mark inner and outer boundaries to apply natural boundary conditions
boundaries = MeshFunction('size_t', mesh, mesh.topology().dim() - 1)
boundaries.set_all(10)
Inner().mark(boundaries, 1)
Outer().mark(boundaries, 2)

# Define a boundary measure with circle boundary tagged
dss = ds(subdomain_data=boundaries)

# Define the element
P3 = FiniteElement("P", triangle, degree = 3)
element = MixedElement([P3] + [P3])

# Define the discrete function space
Q = FunctionSpace(mesh, element)

# Define the Trial and Test function
q, qt = TrialFunction(Q), TestFunction(Q)
v, psi = split(qt) # test function for the transverse displacement and airy stress, respectively

# Create a function to store the solution
q_ = Function(Q)
w_, ph_ = split(q_)

# Define the Dirichlet boundary conditions
bc_w = DirichletBC(Q.sub(0), Constant(0.0), "on_boundary")
bc_ph = DirichletBC(Q.sub(1), Constant(0.0), Inner())
bcs = [bc_w, bc_ph]

# Define the Airy differential operator
airy = lambda f: div(grad(f))*Identity(2) - grad(grad(f))

# Define N, M, Phi and W tensors. See the reference article for details.
N = lambda ph: airy(ph)
M = lambda w: grad(grad(w)) + nu*airy(w)
Ph = lambda ph: (1.0/c_nu)*(grad(grad(ph)) - nu*airy(ph))
W = lambda w: 0.5*(-inner(grad(w), grad(w))*Identity(2) + outer(grad(w), grad(w)))

# Define the new functional. See the reference article for details.
bending = 0.5*inner(M(w_), grad(grad(w_)))*dx # bending energy
membrane = 0.5*inner(Ph(ph_), grad(grad(ph_)))*dx # membrane energy
coupling = 0.5*inner(N(ph_), outer(grad(w_), grad(w_)))*dx # compatibility coupling term
functional = bending - membrane + coupling

# Define the Continuous/Discontinuous Galerkin (CDG) jump forms on the inner edges
jw = lambda v, w: - jump(grad(v), n)*avg(inner(M(w), outer(n, n)))*dS - jump(grad(w), n)*avg(inner(M(v), outer(n, n)))*dS
jph = lambda psi, ph: - jump(grad(psi), n)*avg(inner(Ph(ph), outer(n, n)))*dS - jump(grad(ph), n)*avg(inner(Ph(psi), outer(n, n)))*dS
jc = lambda f, g: avg(inner(W(f), outer(n, n)))*jump(grad(g), n)*dS
js = lambda ft, f, eta: eta('+')/h_avg*jump(grad(ft), n)*jump(grad(f), n)*dS

# Define the CDG forms on the boundary for the weak enforcement of the boundary condition on the derivatives of the primal variables
bc_w_outer = lambda v, w: - inner(grad(v), n)*inner(M(w), outer(n, n))*dss(2) - inner(grad(w), n)*inner(M(v), outer(n, n))*dss(2)
bc_w_inner = lambda v, w: - inner(grad(v), n)*inner(M(w), outer(n, n))*dss(1) - inner(grad(w), n)*inner(M(v), outer(n, n))*dss(1)
bc_ph_inner = lambda psi, ph: - inner(grad(psi), n)*inner(Ph(ph), outer(n, n))*dss(1) - inner(grad(ph), n)*inner(Ph(psi), outer(n, n))*dss(1)

# Define the CDG stabilisation forms on the boundary
s_w_outer = lambda v, w: Constant(1.E3)/h*inner(grad(v), n)*inner(grad(w), n)*dss(2)
s_w_inner = lambda v, w: Constant(1.E3)/h*inner(grad(v), n)*inner(grad(w), n)*dss(1)
s_ph_inner = lambda psi, ph: Constant(1.E3)/h*inner(grad(psi), n)*inner(grad(ph), n)*dss(1)

# Compute the variational forms
a_w = derivative(functional, w_, v) + jw(w_, v)
a_ph = derivative(functional, ph_, psi) - jph(ph_, psi) + jc(w_, psi)
a_bc = bc_w_outer(w_, v) + bc_w_inner(w_, v) + bc_ph_inner(ph_, psi)

# Define the linear form
L = f*v*dx - dot(dot(N(psi), n), ub)*dss(2)

# Define the stabilisation term
stabilisation = js(v, w_, eta_w) + js(psi, ph_, eta_ph) + s_w_outer(v, w_) + s_w_inner(v, w_) + s_ph_inner(psi, ph_)

# Define the residual
F = a_w + a_ph + a_bc - L + stabilisation

# Compute the jacobian
J = derivative(F, q_, q)

# Define the nonlinear variational problem
problem = NonlinearVariationalProblem(F, q_, bcs, J = J)

# Define the in-plane displacement load steps
u_steps = np.linspace(0.0, 11.0, 350)

# Set solver and solver parameters
solver = NonlinearVariationalSolver(problem)
prm = solver.parameters
prm["newton_solver"]["absolute_tolerance"] = 1E-7
prm["newton_solver"]["relative_tolerance"] = 1E-7

# Define the output directory
out_dir = "./"

# Initialise list to store the L2 norms of the solutions
ub_list = []
wL2_list, phL2_list = [], []

# Solve
for count, i in enumerate(u_steps):

	print('------------------------------------------------')
	print('Now solving load increment ' + repr(count) + ' of ' + repr(len(u_steps)))

	# Update the boundary displacement
	ub.U = i
	ub_list.append(i)

	# Solve
	solver.solve()
	w_h, ph_h = q_.split(deepcopy=True)

	# Store L2 norms
	wL2_list.append(norm(w_h, 'l2'))		
	phL2_list.append(norm(ph_h, 'l2'))

# Rescale U with the thickness
thickness = 0.01
ub_scaledlist = [thickness*i for i in ub_list]

# Plot w vs U
fig = plt.figure(figsize=(5.0, 5.0/1.648))
plt.plot(wL2_list, ub_scaledlist, "-", linewidth=3.5, color='darkred', label=r"$|| w ||_{L^2(\Omega)} \, / \, t$")
plt.xlabel(r"$|| w ||_{L^2(\Omega)} \, / \, t$", fontsize=12)
plt.ylabel(r"$U \, / \, t $", fontsize=12)
plt.tight_layout()
plt.grid()
plt.savefig(out_dir + "w_plot.png")
plt.savefig(out_dir + "w_plot.pdf")

# Plot phi vs U
fig = plt.figure(figsize=(5.0, 5.0/1.648))
plt.plot(phL2_list, ub_scaledlist, "-", linewidth=3.5, color='darkred', label=r"$|| \phi ||_{L^2(\Omega)} \, / \, t$")
plt.xlabel(r"$|| \phi ||_{L^2(\Omega)} \, / \, D$", fontsize=12)
plt.ylabel(r"$U \, / \, t $", fontsize=12)
plt.tight_layout()
plt.grid()
plt.savefig(out_dir + "ph_plot.png")
plt.savefig(out_dir + "ph_plot.pdf")